/**
 * Test class for VegetarianSandwich.java
 * @author Nathan Bokobza
 * @version 18/10/2023
 */

package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


/**
 * Unit test for VegetarianSandwich Class
 */
public class VegetarianSandwichTest 
{
    /**
     * Returns true if the constuctor works correctly
     */
    @Test
    public void constructorInitializationTest()
    {
        VegetarianSandwich vs = new VegetarianSandwich();
        assertEquals("", vs.getFilling());
    }

    /**
     * Returns true if the class rejects adding
     * non-vegetarian ingredients
     */
    @Test
    public void invalidFillingsTest()
    {
        VegetarianSandwich vs = new VegetarianSandwich();
        String[] illegalFillings = {"chicken", "beef", "fish", "meat", "pork"};

        for (int i = 0; i < illegalFillings.length; i++)
        {
            try{
                vs.addFilling(illegalFillings[i]);
                assertTrue(false);
            }
            catch (Exception e)
            {
                System.out.println(illegalFillings[i] + " Throws exception correctly.");
            }
        }

        assertTrue(true);
    }

    /**
     * Returns true if sandwhich isn't vegan as expected
     */
    @Test
    public void veganValidityTest()
    {
        VegetarianSandwich vs = new VegetarianSandwich();
        vs.addFilling("egg");
        assertTrue(!vs.isVegan());
    }

    /**
     * Returns true if the filling adds and displays correctly
     */
    @Test
    public void singleFillingTest()
    {
        VegetarianSandwich vs = new VegetarianSandwich();
        vs.addFilling("cheese");
        assertEquals(vs.getFilling(), "cheese");
    }

    /**
     * Returns true if the fillings add and displays correctly
     */
    @Test
    public void severalFillingsTest()
    {
        VegetarianSandwich vs = new VegetarianSandwich();
        vs.addFilling("egg");
        vs.addFilling("cheese");
        assertEquals(vs.getFilling(), "egg, cheese");
    }


    /**
     * Returns true if the isVegetarian function returns true
     * as it is supposed to.
     */
    @Test
    public void isVegetarianTest()
    {
        VegetarianSandwich vs = new VegetarianSandwich();
        assertTrue(vs.isVegetarian());
    }
}