package dawson;

import static org.junit.Assert.*;

import org.junit.Test;


public class BagelSandwichTest 
{

    /**
     * tests the constructor
     */
    @Test
    public void ConstructorTest(){

        BagelSandwich b = new BagelSandwich();
        assertEquals("", b.getFilling());

    }

    /**
     * tests addFilling() when adding one filling
     */
    @Test
    public void addFillingTestOneFilling(){

        BagelSandwich b = new BagelSandwich();

        b.addFilling("Peanut butter");

        assertEquals("Peanut butter", b.getFilling());

    }

    /**
     * tests addFilling() when adding three fillings
     */
    @Test
    public void addFillingTestThreeFillings(){

        BagelSandwich b = new BagelSandwich();

        b.addFilling("Peanut butter");
        b.addFilling("Jelly");
        b.addFilling("Despair");

        assertEquals("Peanut butter, Jelly, Despair", b.getFilling());

    }



    /**
     * tests isVegetarian()
     */
    @Test
    public void isVegetarianTest(){

        BagelSandwich b = new BagelSandwich();

        try{
            boolean t = b.isVegetarian();
            fail("no error occured when one was expected");
        }
        catch(UnsupportedOperationException e){

        }
    }
}
