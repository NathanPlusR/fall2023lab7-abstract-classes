/**
 * Bagel Skeleton Class
 * @author Nathan Bokobza
 * @version 18/10/2023
 */

package dawson;

public class BagelSandwich implements ISandwhich{
    
    private String filling;

    /**
     * BagelSandwhich Constructor
     */
    public BagelSandwich()
    {
        throw new UnsupportedOperationException("Not written yet");
    }

    /**
     * Getter method for filling field
     * @return Returns filling field
     */
    public String getFilling()
    {
        throw new UnsupportedOperationException("Not written yet");
    }

    /**
     * Method to append given topping to filling field
     * @param topping to be appeneded
     */
    public void addFilling(String topping)
    {
        throw new UnsupportedOperationException("Not written yet");
    }

    /**
     * Checks if sandwhich is vegetarian
     * allways throws an exception
     */
    public boolean isVegetarian()
    {
        throw new UnsupportedOperationException("Not written yet");
    }
}
