package dawson;

public class VegetarianSandwich implements ISandwhich{
    private String filling;

    /**
     * constructor.
     */
    public VegetarianSandwich(){
        throw new UnsupportedOperationException("not written");
    };

    /**
     * getter method.
     * @returns this.filling
     */
    public String getFilling(){
        throw new UnsupportedOperationException("not written");
    };

    /**
     * adds to the filling field
     * @param topping the topping to be added
     */
    public void addFilling(String topping){
        throw new UnsupportedOperationException("not written");
    }

    /**
     * states weather or not the sandwhich is vegetarian. could really be a field.
     * @returns true if vegetairan, false otherwise. aka returns true.
     */
    public boolean isVegetarian(){
        throw new UnsupportedOperationException("not written");
    };

    /**
     * determines weather or not the sandwhich is vegan based on its fillings
     * @returns true if the fillings do not contain "cheese" or "egg", otherwise false.
     */
    public boolean isVegan(){
        throw new UnsupportedOperationException("not written");
    };
}
