package dawson;

public interface ISandwhich {
    
    public String getFilling();

    public void addFilling(String topping);

    public boolean isVegetarian();
}
